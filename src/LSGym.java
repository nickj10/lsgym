import controller.ControladorButtons;
import controller.ControladorRadioButtons;
import model.Activitat;
import model.ActivitiesManager;
import model.JsonCreator;
import view.Finestra;

import javax.swing.*;


public class LSGym {
    public static void main(String args[]) {
        int i = 0;

        // Carreguem la informació de les activitats del JSON
        JsonCreator jsReader = new JsonCreator();
        Activitat[] activitats = jsReader.getActivities();
        for(i = 0; i < activitats.length; i++) {
            activitats[i].convertToTime();
        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // crea la vista
                Finestra vista = new Finestra();

                // creem el model
                ActivitiesManager lsGym = new ActivitiesManager(activitats);
                vista.refreshMyActivityView(lsGym.getActivitatsSorted());

                // crea el controlador del botons i estableix la relacio C->V i C->M
                ControladorButtons controladorButtons = new ControladorButtons(vista, lsGym);
                ControladorRadioButtons controladorRadioButtons = new ControladorRadioButtons(vista, lsGym);

                // establim la relacio V--->C
                vista.assignButtonControl(controladorButtons, activitats.length);
                vista.assignRadioButtonControl(controladorRadioButtons);

                // fem la vista visible
                vista.setVisible(true);
            }
        });
    }
}
