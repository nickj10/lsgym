package controller;

import model.ActivitiesManager;
import view.Finestra;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControladorButtons implements ActionListener {
    private Finestra vista;
    private ActivitiesManager model;

    public ControladorButtons(Finestra vista, ActivitiesManager model) {
        // Establim la "relació" C->V
        this.vista = vista;
        this.model = model;
    }

    public void actionPerformed(ActionEvent e) {
        // Recuperem el text del botó
        String quinBoto = ((JButton)e.getSource()).getActionCommand();
        if (quinBoto.equals("RESET")) {
            // Recuperem el text que ha introduït l'usuari
            String filename = vista.getJtfReset().getText();
            if(model.createActivitiesJson(filename)) {
                model.resetActivities();
                vista.refreshButton();
                if (vista.getJrbShowAll().isSelected()) {
                    vista.setButtonCount(0);
                    vista.refreshActivitiesPanels(model.getActivitatsSorted());
                } else {
                    if (vista.getJrbShowInComing().isSelected()) {
                        vista.setButtonCount(model.filterInComing());
                        vista.refreshActivitiesPanels(model.getActivitatsInComing());
                    }
                }

                // Mostrem el misstage d'èxit
                vista.showResetSuccessful();
            } else {
                // Mostrem el misstage d'error
                vista.showResetError();
            }
            vista.getJtfReset().setText("");
        } else if (quinBoto.contains("Apply")) {
            // Depèn de l'activitat, agafem el id de l'activitat per veure quin és
            String[] partition = quinBoto.split("_");
            int id = Integer.parseInt(partition[1]);

            // Busquem el index de l'activitat en l'array original
            int index = model.getIndexActivitat(model.getActivitatsSorted(), model.buscaActivitatSorted(id));
            model.applyForActivity(model.buscaActivitatSorted(id));

            // Mirem si ja s'han acabat les places
            if (model.outOfPlaces(model.buscaActivitatSorted(id))) {
                // Agafem l'index de l'activitat de la llista original
                vista.deactivateApplyButton(index);
            }
            if (vista.getJrbShowAll().isSelected()) {
                // Refresquem les places que es veuen a la vista
                vista.refreshPlaces(model.getActivitatsSorted().get(index), index);
                vista.refreshActivityPanelColor(model.getActivitatsSorted());
            } else {
                if (vista.getJrbShowInComing().isSelected()) {
                    // Refresquem les places que es veuen a la vista
                    vista.refreshPlaces(model.getActivitatsSorted().get(index), index);
                    vista.refreshActivityPanelColor(model.getActivitatsInComing());
                }
            }

        }
    }
}
