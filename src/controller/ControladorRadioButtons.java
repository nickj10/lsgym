package controller;

import model.ActivitiesManager;
import view.Finestra;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControladorRadioButtons implements ActionListener {
    private Finestra vista;
    private ActivitiesManager model;

    public ControladorRadioButtons(Finestra vista, ActivitiesManager model) {
        this.vista = vista;
        this.model = model;
        this.vista.refreshActivitiesPanels(model.getActivitatsSorted());
    }

    public void actionPerformed(ActionEvent e) {
        String comanda = vista.getShowButtonsGroup().getSelection().getActionCommand();
        if (comanda.equals("Show all")) {
            // Mostrem totes les activitats
            vista.setButtonCount(0);
            vista.refreshActivitiesPanels(model.getActivitatsSorted());
        } else {
            if (comanda.equals("Show in-coming")) {
                // Mostrem només les activitats posteriors a la hora actual
                vista.setButtonCount(model.filterInComing());
                vista.refreshActivitiesPanels(model.getActivitatsInComing());
            }
        }
    }
}
