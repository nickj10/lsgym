package view;

import controller.ControladorButtons;
import controller.ControladorRadioButtons;
import model.Activitat;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.LinkedList;

public class Finestra extends JFrame {
    private JRadioButton jrbShowAll;
    private JRadioButton jrbShowInComing;
    private JTextField jtfReset;
    private JButton jbReset;
    private JPanel[] jpActivities;
    private JScrollPane jspActivities;
    private JPanel jpGeneralActivities;
    private JButton[] jbApply;
    private JLabel[] jlSeating;
    private int buttonCount;
    private ButtonGroup showButtonsGroup;

    public Finestra() {
        int i;
        buttonCount = 0;

        // Panell superior
        JPanel jpSuperior = new JPanel(new FlowLayout());
        showButtonsGroup = new ButtonGroup();

        // Crear els radio buttons
        jrbShowAll = new JRadioButton("Show all");
        jrbShowAll.setActionCommand("Show all");
        jrbShowInComing = new JRadioButton("Show in-coming");
        jrbShowInComing.setActionCommand("Show in-coming");
        jrbShowAll.setSelected(true); // per defecte
        jrbShowInComing.setSelected(false);

        // Afegir els radio buttons al grup
        showButtonsGroup.add(jrbShowAll);
        showButtonsGroup.add(jrbShowInComing);

        jpSuperior.add(jrbShowAll);
        jpSuperior.add(jrbShowInComing);
        getContentPane().add(jpSuperior, BorderLayout.PAGE_START);

        // Panell central
        JPanel jpCentral = new JPanel();
        jpCentral.setLayout(new GridLayout());

        // Aux 1 del panell central
        jpGeneralActivities = new JPanel();
        jpGeneralActivities.setBorder(BorderFactory.createTitledBorder("Activities"));
        jpGeneralActivities.setLayout(new BoxLayout(jpGeneralActivities, BoxLayout.Y_AXIS));
        jspActivities = new JScrollPane(jpGeneralActivities);
        jpCentral.add(jspActivities);


        getContentPane().add(jpCentral, BorderLayout.CENTER);

        // Panell Inferior
        JPanel jpInferior = new JPanel(new FlowLayout());
        jbReset = new JButton("RESET");
        jbReset.setActionCommand("RESET");
        jtfReset = new JTextField("");
        jtfReset.setPreferredSize(new Dimension(400, 25));
        jtfReset.setEditable(true);
        jpInferior.add(jtfReset);
        jpInferior.add(jbReset);
        getContentPane().add(jpInferior, BorderLayout.PAGE_END);


        // Donem una mida a la finestra
        setSize(500, 600);
        setResizable(false);

        // Li posem un títol
        setTitle("LSGym");

        // Indiquem que s'aturi el programa si fab clic a la "X" de la finestra
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        // Indiquem que la finestra se situï al centre de la pantalla
        setLocationRelativeTo(null);
    }

    public void refreshActivitiesPanels(LinkedList<Activitat> activitats) {
        int i;
        // Hem de treure els components anteriors per poder posar la nova informació
        jpGeneralActivities.removeAll();

        for (i = 0; i < activitats.size(); i++) {
            Activitat a = activitats.get(i);
            jpActivities[i] = new JPanel(new GridLayout(2, 1));
            TitledBorder border = BorderFactory.createTitledBorder(a.getNom() + " (" + a.getId() + ")");
            jpActivities[i].setBorder(border);
            jpActivities[i] = setBorderColor(jpActivities[i],a);

            // Primera línia de informació
            JPanel jpAux = new JPanel();
            jpAux.setLayout(new BorderLayout());
            JLabel jlStart = new JLabel("Starts at " + a.getInici());
            jlSeating[buttonCount].setText((a.getMaxAforament() - a.getSignedUp()) + " remaining places");
            jlSeating[buttonCount].setHorizontalAlignment(JLabel.CENTER);
            JLabel jlDuration = new JLabel("Duration: " + a.getTotalDuration() + " h");
            jpAux.add(jlStart, BorderLayout.LINE_START);
            jpAux.add(jlSeating[buttonCount]);
            jpAux.add(jlDuration, BorderLayout.LINE_END);

            // Segona línia on està el botó
            JPanel jpAux2 = new JPanel();
            jpAux2.add(jbApply[buttonCount++]);

            // Afegir els elements del panell d'activitats
            jpActivities[i].add(jpAux);
            jpActivities[i].add(jpAux2);
            jpGeneralActivities.add(this.jpActivities[i]);

        }
        // Refresca el panell amb els nous canvis
        jpGeneralActivities.revalidate();
    }

    public void refreshMyActivityView(LinkedList<Activitat> activitats) {
        int i = 0;
        // Inicialització dels arrays
        jpActivities = new JPanel[activitats.size()];
        jbApply = new JButton[activitats.size()];
        jlSeating = new JLabel[activitats.size()];
        for(i = 0; i < activitats.size(); i++) {
            jbApply[i] = new JButton("Apply");
            jbApply[i].setActionCommand("Apply_" + activitats.get(i).getId());
            jpActivities[i] = new JPanel(new GridLayout(2, 1));
            jlSeating[i] = new JLabel();
        }

    }

    public void showResetSuccessful(){
        JOptionPane.showMessageDialog(this,
                "Successfully reset.");
    }

    public void showResetError(){
        JOptionPane.showMessageDialog(this,
                "The filename should not contain spaces.",
                "Warning",
                JOptionPane.WARNING_MESSAGE);
    }

    public void refreshButton() {
        int i;
        for(i = 0; i < jbApply.length; i++) {
            jbApply[i].setEnabled(true);
        }
    }

    public void refreshPlaces(Activitat a, int index) {
        jlSeating[index].setText((a.getMaxAforament() - a.getSignedUp()) + " remaining places");
    }

    public void refreshActivityPanelColor(LinkedList<Activitat> activitats) {
        int i = 0;
        for(i = 0; i < activitats.size(); i++) {
            Activitat a = activitats.get(i);
            jpActivities[i] = setBorderColor(jpActivities[i], a);
        }
        repaint();
    }

    public JPanel setBorderColor (JPanel panel, Activitat a) {
        TitledBorder border = (TitledBorder) panel.getBorder();
        if ((a.getMaxAforament() - a.getSignedUp()) > 5) {
            panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.green), border.getTitle(), border.getTitleJustification(),
                    border.getTitlePosition(), border.getTitleFont(), border.getTitleColor()));
        } else {
            panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.red), border.getTitle(), border.getTitleJustification(),
                    border.getTitlePosition(), border.getTitleFont(), border.getTitleColor()));
        }
        return panel;
    }

    public void deactivateApplyButton(int activityIndex) {
        jbApply[activityIndex].setEnabled(false);
    }

    public void assignButtonControl(ControladorButtons bController, int numApplyButtons) {
        int i = 0;
        // Registra els text buttons
        jbReset.addActionListener(bController);

        for (i = 0; i < jpActivities.length; i++) {
            jbApply[i].addActionListener(bController);
        }
    }

    public void assignRadioButtonControl(ControladorRadioButtons bController) {
         //Registra els radio buttons
        jrbShowAll.addActionListener(bController);
        jrbShowInComing.addActionListener(bController);
    }

    public JRadioButton getJrbShowAll() {
        return jrbShowAll;
    }

    public void setJrbShowAll(JRadioButton jrbShowAll) {
        this.jrbShowAll = jrbShowAll;
    }

    public JRadioButton getJrbShowInComing() {
        return jrbShowInComing;
    }

    public void setJrbShowInComing(JRadioButton jrbShowInComing) {
        this.jrbShowInComing = jrbShowInComing;
    }

    public JTextField getJtfReset() {
        return jtfReset;
    }

    public void setJtfReset(JTextField jtfReset) {
        this.jtfReset = jtfReset;
    }

    public JButton getJbReset() {
        return jbReset;
    }

    public void setJbReset(JButton jbReset) {
        this.jbReset = jbReset;
    }

    public JPanel[] getJpActivities() {
        return jpActivities;
    }

    public void setJpActivities(JPanel[] jpActivities) {
        this.jpActivities = jpActivities;
    }

    public JScrollPane getJspActivities() {
        return jspActivities;
    }

    public void setJspActivities(JScrollPane jspActivities) {
        this.jspActivities = jspActivities;
    }

    public JPanel getJpGeneralActivities() {
        return jpGeneralActivities;
    }

    public void setJpGeneralActivities(JPanel jpGeneralActivities) {
        this.jpGeneralActivities = jpGeneralActivities;
    }

    public JButton[] getJbApply() {
        return jbApply;
    }

    public void setJbApply(JButton[] jbApply) {
        this.jbApply = jbApply;
    }

    public JLabel[] getJlSeating() {
        return jlSeating;
    }

    public void setJlSeating(JLabel[] jlSeating) {
        this.jlSeating = jlSeating;
    }

    public int getButtonCount() {
        return buttonCount;
    }

    public void setButtonCount(int buttonCount) {
        this.buttonCount = buttonCount;
    }

    public ButtonGroup getShowButtonsGroup() {
        return showButtonsGroup;
    }

    public void setShowButtonsGroup(ButtonGroup showButtonsGroup) {
        this.showButtonsGroup = showButtonsGroup;
    }

}
