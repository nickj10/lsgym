package model;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Activitat implements Comparable<Activitat> {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String nom;
    @SerializedName("start")
    private String inici;
    @SerializedName("duration")
    private int minuts;
    @SerializedName("seating")
    private int maxAforament;
    private transient Date iniciTime;
    private int signedUp;

    public void convertToTime() {
        try {
            SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
            this.iniciTime = parser.parse(this.inici);
        } catch (ParseException e) {
            System.out.println("There has been an error in parsing the start time.");
        }
    }

    public String getTotalDuration() {
        String hores;
        String minuts;
        int hours = this.minuts / 60; //since both are ints, you get an int
        int minutes = this.minuts % 60;

        // Convertir les hores en un string depenent del nombre de hores per l'estètica
        if (hours < 10) {
            hores = "0" + hours;
        }
        else {
            hores = Integer.toString(hours);
        }

        // Convertir els minuts en un string depenent del nombre de hores per l'estètica
        if (minutes < 10) {
            minuts = "0" + minutes;
        }
        else {
            minuts = Integer.toString(minutes);
        }
        return (hores + ":" + minuts);
    }

    @Override
    public int compareTo(Activitat a) {
        return this.getIniciTime().compareTo(a.getIniciTime());
    }

    public void incrementaApuntats() {
        signedUp++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getInici() {
        return inici;
    }

    public void setInici(String inici) {
        this.inici = inici;
    }

    public int getMinuts() {
        return minuts;
    }

    public void setMinuts(int minuts) {
        this.minuts = minuts;
    }

    public int getMaxAforament() {
        return maxAforament;
    }

    public void setMaxAforament(int maxAforament) {
        this.maxAforament = maxAforament;
    }

    public Date getIniciTime() {
        return iniciTime;
    }

    public void setIniciTime(Date iniciTime) {
        this.iniciTime = iniciTime;
    }

    public int getSignedUp() {
        return signedUp;
    }

    public void setSignedUp(int signedUp) {
        this.signedUp = signedUp;
    }

}
