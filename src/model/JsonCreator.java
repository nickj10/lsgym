package model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.LinkedList;

public class JsonCreator {
    private Activitat[] activitats;

    public Activitat[] getActivities () {
        Gson gson = new Gson();
        try {
            BufferedReader br = new BufferedReader(new FileReader("registers/activities.json"));
            this.activitats = gson.fromJson(br, Activitat[].class);
            return this.activitats;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean createJson(LinkedList<Activitat> activities, String filename) {
        int i;
        try {
            Activitat[] activitats = activities.toArray(new Activitat[activities.size()]);
            Gson gson = new GsonBuilder().create();
            FileWriter fileWriter = new FileWriter("registers/"+filename + ".json");
            String json = gson.toJson(activities);
            fileWriter.write(json);
            fileWriter.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public Activitat[] getActivitats() {
        return activitats;
    }

    public void setActivitats(Activitat[] activitats) {
        this.activitats = activitats;
    }
}
