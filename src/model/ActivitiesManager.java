package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ActivitiesManager {
    private LinkedList<Activitat> activitatsSorted;
    private LinkedList<Activitat> activitatsInComing;
    private Activitat[] activitatsOriginals;

    public ActivitiesManager(Activitat[] activitats) {
        int i = 0;
        activitatsSorted = new LinkedList<Activitat>(Arrays.asList(activitats));
        activitatsInComing = new LinkedList<Activitat>(Arrays.asList(activitats));
        activitatsOriginals = activitats;
        Collections.sort(activitatsSorted);
    }

    public void resetActivities() {
        activitatsSorted = new LinkedList<Activitat>(Arrays.asList(activitatsOriginals));
        for (Activitat a: activitatsSorted) {
            a.setSignedUp(0);
        }
        Collections.sort(activitatsSorted);
    }

    public int filterInComing(){
        int i = 0;
        int buttonCount = 0;
        try {
            // Eliminar tots els elements de la llista
            while (!activitatsInComing.isEmpty()) {
                activitatsInComing.removeFirst();
            }

            // Filtrar les activitats
            for (i = 0; i < activitatsSorted.size(); i++) {
                Activitat a = activitatsSorted.get(i);
                // Agafar la hora ara i comparar-la amb la hora d'inici de l'activitat
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
                Date timeNow = new Date();
                String timeRightNow = dateFormat.format(timeNow);
                Date d1 = dateFormat.parse(timeRightNow);
                Date d2 = dateFormat.parse(a.getInici());
                long elapsed = d2.getTime() - d1.getTime();

                // Si l'inici és posterior, la mostrem
                if (elapsed > 0) {
                    if (activitatsInComing.isEmpty()) {
                        buttonCount = i;
                    }
                    activitatsInComing.add(a);
                }
            }

        } catch(ParseException e) {
            System.out.println("Error, hi ha un error en fer el parsing de les dates.");
        }
        return buttonCount;
    }

    public Activitat buscaActivitatSorted(int id) {
        for(Activitat a: activitatsSorted) {
            if (a.getId() == id) {
                return a;
            }
        }
        return null;
    }

    // Retorna l'index de l'activitat
    public int getIndexActivitat(LinkedList<Activitat> activitats, Activitat activitat) {
        int i = 0;
        for(i = 0; i < activitats.size(); i++) {
            Activitat a = activitats.get(i);
            if (a.equals(activitat)) {
                return i;
            }
        }
        return -1;
    }

    public void applyForActivity(Activitat activity) {
        int i = 0;
        // Busquem l'activitat a la llista
        for (i = 0; i < activitatsSorted.size(); i++) {
            Activitat a = activitatsSorted.get(i);
            if (a.getNom().equals(activity.getNom())) {
                activitatsSorted.get(i).incrementaApuntats();
            }
        }
    }

    public boolean outOfPlaces(Activitat activity) {
        return activity.getSignedUp() == activity.getMaxAforament();
    }

    public boolean createActivitiesJson(String filename) {
        JsonCreator jsonCreator = new JsonCreator();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");
        Date now = new Date();
        String rightNow = dateFormat.format(now);
        if (filename.isEmpty()) {
            filename = rightNow;
            return jsonCreator.createJson(this.activitatsSorted, filename);
        } else {
            if(!filename.contains(" ")) {
                return jsonCreator.createJson(this.activitatsSorted, filename);
            }
        }
        return false;
    }

    // Getters and setters
    public int getNumActivitats() {
        return activitatsSorted.size();
    }

    public LinkedList<Activitat> getActivitatsSorted() {
        return activitatsSorted;
    }

    public void setActivitatsSorted(LinkedList<Activitat> activitatsSorted) {
        this.activitatsSorted = activitatsSorted;
    }

    public LinkedList<Activitat> getActivitatsInComing() {
        return activitatsInComing;
    }

    public void setActivitatsInComing(LinkedList<Activitat> activitatsInComing) {
        this.activitatsInComing = activitatsInComing;
    }

    public Activitat[] getActivitatsOriginals() {
        return activitatsOriginals;
    }

    public void setActivitatsOriginals(Activitat[] activitatsOriginals) {
        this.activitatsOriginals = activitatsOriginals;
    }
}
